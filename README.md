# README #

### What is this repository for? ###

This application is a project for the "Modelling and Simulation" class on Faculty of Information Technology of Brno University of Technology.
Task definition (only in Czech) : 

5. Model sítě hasičských stanic

Uvažujme ohraničený 2D prostor (mapu), kde jsou souřadnice vyjádřeny v metrech a vzdálenost mezi dvěma body lze určit Euklidovsky (lze vypočítat dobu cesty mezi dvěma body). Do prostoru umístěte N hasičských stanic (1 vůz do každé). V prostoru bude docházet v intervalech daných exp rozložením se středem X k požárům v náhodně generovaných místech. Každý požár nechť má náhodně generovanou intenzitu 1-3 (rovnoměrně). Intenzita požáru určuje počet hasičských vozů, které k požáru musí přijet. Navrhněte a modelujte proces požáru a proces hašení (se zapojením počtu hasičů). V rámci modelu požáru modelujte škody vzniklé hořením a ovlivněné dobou hašení (např. požár intenzity 3 lze uhasit pouze s třemi vozy). Modelujte provozní náklady na udržování stanic. Experimentálně zjistěte optimální počet stanic a jejich rozmístění (náklady na provoz stanic versus škody z požárů).

### How do I get set up? ###

Required libraries :

  * simlib (http://www.fit.vutbr.cz/~peringer/)

All you should need to do next is run make and make run to run test scenarios.