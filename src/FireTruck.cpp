/**
 * @project VUT FIT IMS-2016
 * @file FireTruck.cpp
 * @author Tomas Polasek, Jan Pokorny
 * @brief This class represents fire fighting truck.
 */

#include "FireTruck.h"

namespace fs
{
    FireTruck::FireTruck(Point basePos) :
        statTimeOutOfBase("Time out of base for a single truck"),
        mBasePosition(basePos), mCurPosition(basePos), mDestination(basePos),
        mAtBase{true}
    {
    }

    void FireTruck::Behavior()
    {
        Passivate();

        double waitStartTime = Time;

        while (true)
        {

            g::map.statTrucksAtBase(Time - waitStartTime);

            double dispatchTime{Time};
            mAtBase = false;
            g::map.statTrucksLeft++;

            DPrint("Leaving for fire site... (%p)\n", mFireSite);
            Wait(travelTime());
            g::map.statTrucksDriving(Time - dispatchTime);

            DPrint("Arriving at the fire...(%p)\n", mFireSite);
            mFireSite->truckArrived();

            double timeToAction{Exponential(TIME_TO_ACTION)};
            g::map.statTimeToAction(timeToAction);
            Wait(timeToAction);

            DPrint("Ready for extinguishing...\n");
            g::map.statTimeFromDispatchToAction(Time - dispatchTime);

            mFireSite->truckReady();

            // Wait for the end of extinguishing.
            Passivate();

            Wait(Exponential(TIME_TO_PACK_UP));

            DPrint("Returning to base...\n");
            Wait(travelTime());
            g::map.statTrucksOutOfBase(Time - dispatchTime);
            statTimeOutOfBase(Time - dispatchTime);

            // Preparation time before the next action.
            Wait(PREP_TIME);

            mAtBase = true;
            g::map.statTrucksReturned++;
            g::map.returnFireTruck(this);

            waitStartTime = Time;

            // Wait at the base.
            Passivate();
        }
    }

    void FireTruck::dispatch(FireSite *site)
    {
        if (site->trucksRequired() == 0)
        {
            throw std::runtime_error("Cannot assist already full fire site!");
        }

        mFireSite = site;
        setDestination(site->position());
        site->addParticipant(this);
        Activate();
    }

    void FireTruck::returnToBase()
    {
        mFireSite = nullptr;
        setDestination(mBasePosition);
        Activate();
    }
}

