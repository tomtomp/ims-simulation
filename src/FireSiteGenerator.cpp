/**
 * @project VUT FIT IMS-2016
 * @file FireSiteGenerator.cpp
 * @author Tomas Polasek, Jan Pokorny
 * @brief Generator for the fire sites.
 */

#include "FireSiteGenerator.h"

namespace fs
{
    FireSiteGenerator::FireSiteGenerator()
    {  }

    void FireSiteGenerator::Behavior()
    {
        FireSite *fireSite{new FireSite(g::map.uniPoint(), genIntensity(), genValue(), genDiscoveryDelta())};
        fireSite->Activate();
        DPrint("Created new fire site (%p)...\n", fireSite);
        g::map.addFireSite(fireSite);
        double waitTime(Exponential(g::map.fireDelay()));
        g::map.statTimeBetweenFires(waitTime);
        Activate(Time + waitTime);
    }

    std::size_t FireSiteGenerator::genIntensity() const
    {
        double val{Random()};
        return static_cast<std::size_t >(val * MAX_INTENSITY + 1.0);
        //return val < 0.5 ? 1 : (val < 0.85 ? 2 : 3);
    }

    double FireSiteGenerator::genValue() const
    {
        return Exponential(AVG_VALUE_OF_SITE);
    }

    double FireSiteGenerator::genDiscoveryDelta() const
    {
        return Exponential(AVG_DISCOVERY_TIME);
    }
}
