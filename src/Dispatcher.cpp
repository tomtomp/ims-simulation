/**
 * @project VUT FIT IMS-2016
 * @file Dispatcher.cpp
 * @author Tomas Polasek, Jan Pokorny
 * @brief Dispatcher for dispatching fire fighting trucks.
 */

#include "Dispatcher.h"

namespace fs
{
    Dispatcher::Dispatcher()
    { }

    void Dispatcher::Behavior()
    {
        bool done{false};
        Queue &fireSites{g::map.fireSites()};

        while (!done && fireSites.size())
        {
            //if (fireSites.size() != 0)
            {
                FireSite *site{dynamic_cast<FireSite*>(fireSites.front())};
                DPrint("Searching for trucks for (%p).\n", site);

                std::size_t required{site->trucksRequired()};

                std::vector<FireTruck*> &freeTrucks{g::map.freeFireTrucks()};

                std::sort(freeTrucks.begin(), freeTrucks.end(),
                          [&] (const FireTruck* first, const FireTruck* second) {
                              return first->position().distance(site->position()) >
                                     second->position().distance(site->position());
                          });

                std::size_t toDispatch{required > freeTrucks.size() ? freeTrucks.size() : required};

                for (std::size_t iii = 0; iii < toDispatch; ++iii)
                {
                    FireTruck *truck{freeTrucks.back()};
                    if (truck->predictTravelTime(site->position()) > MAX_TRAVEL_TIME)
                    { // If it would take more than 2 hours, it is too far away!
                        Print("Fire truck is too far w");
                        break;
                    }
                    truck->dispatch(site);
                    freeTrucks.pop_back();
                }

                if (site->trucksRequired() == 0)
                {
                    DPrint("Sent enough fire trucks, removing (%p), remaining %ld...\n", fireSites.front(), fireSites.size());
                    fireSites.GetFirst();
                    DPrint("After removing %ld\n", fireSites.size());
                }
                else
                {
                    done = true;
                }
            }
        }
    }
}

