/**
 * @project VUT FIT IMS-2016
 * @file Globals.cpp
 * @author Tomas Polasek, Jan Pokorny
 * @brief Global variables.
 */

#include "Globals.h"

#include "FireSite.h"
#include "FireTruck.h"
#include "Dispatcher.h"
#include "FireSiteGenerator.h"

namespace fs
{
    namespace g
    {
        Map &map(Map::instance());
        const char *Map::INTENSITY_STAT_NAMES[] = {
            "Burn times for intensity 1",
            "Burn times for intensity 2",
            "Burn times for intensity 3"
        };
        const char *Map::TRUCK_ARRIVAL_STAT_NAMES[] = {
            "Arrival times for first truck",
            "Arrival times for second truck",
            "Arrival times for third truck"
        };

        void Map::init(std::size_t mapSize, double firesPerYear, double originalMapArea, std::size_t numStations)
        {
            prepareMembers(mapSize, firesPerYear, originalMapArea);

            // Generate the fire stations.
            for (decltype(mapSize) iii = 0; iii < numStations; ++iii)
            {
                // Generate a random position for each fire truck.
                FireTruck *truck{new FireTruck(uniPoint())};
                freeFireTrucks().push_back(truck);
                fireTrucks().push_back(truck);
                truck->Activate();
            }

            createEvents();
        }

        void Map::init(std::size_t mapSize, double firesPerYear,
                       double originalMapArea, const std::vector<Point> &stations)
        {
            prepareMembers(mapSize, firesPerYear, originalMapArea);

            for (const Point &p : stations)
            {
                // Generate a random position for each fire truck.
                assert(p.x < mSize && p.y < mSize);
                FireTruck *truck{new FireTruck(p)};
                freeFireTrucks().push_back(truck);
                fireTrucks().push_back(truck);
                truck->Activate();
            }

            createEvents();
        }

        void Map::freeDynamics()
        {
            for (FireTruck *truck : fireTrucks())
            {
                if (truck->isAllocated())
                {
                    delete truck;
                }
            }
            fireTrucks().clear();
            freeFireTrucks().clear();

            if (mDispatcher && mDispatcher->isAllocated())
            {
                delete mDispatcher;
            }
            mDispatcher = nullptr;

            if (mFireSiteGenerator && mFireSiteGenerator->isAllocated())
            {
                delete mFireSiteGenerator;
            }
            mFireSiteGenerator = nullptr;
        }

        void Map::resetStatistics()
        {
            statTrucksDriving.SetName("Time spent driving");
            statTrucksDriving.Clear();
            statTrucksOutOfBase.SetName("Time spent out of base");
            statTrucksOutOfBase.Clear();
            statTrucksAtBase.SetName("Time spent at base");
            statTrucksAtBase.Clear();
            statExtinguishLength.SetName("Time spent extinguishing");
            statExtinguishLength.Clear();
            statTimeBeforeArrival.SetName("Time before fire trucks arrived");
            statTimeBeforeArrival.Clear();
            statTimeBeforeAvailable.SetName("Time before all fire trucks are available");
            statTimeBeforeAvailable.Clear();
            statTimeBetweenFires.SetName("Time between starting fires");
            statTimeBetweenFires.Clear();
            statTimeToAction.SetName("Time before action");
            statTimeToAction.Clear();
            statTimeFromDispatchToAction.SetName("Time between dispatch and action ready");
            statTimeFromDispatchToAction.Clear();
            statFireTruckLate.SetName("By how much was a fire truck late");
            statFireTruckLate.Clear();
            statDamage.SetName("How much damage has the fire done");
            statDamage.Clear();
            statDamagePrevented.SetName("How much damage has been prevented");
            statDamagePrevented.Clear();
            statDamagePercentage.SetName("Damage as a percentage of total value.");
            statDamagePercentage.Clear();
            statValue.SetName("Value of the fire sites in KC");
            statValue.Clear();

            for (std::size_t iii = 0; iii < MAX_INTENSITY; ++iii)
            {
                statsFireBurnLength[iii].SetName(INTENSITY_STAT_NAMES[iii]);
                statsFireBurnLength[iii].Clear();
            }
            for (std::size_t iii = 0; iii < MAX_INTENSITY; ++iii)
            {
                statsTruckArrived[iii].SetName(TRUCK_ARRIVAL_STAT_NAMES[iii]);
                statsTruckArrived[iii].Clear();
            }

            /*
            histFirePosX.SetName("Fire site x positions");
            histFirePosX.Init(0.0, 100, mSize * TILE_SIZE / 100.0 + 1);
            histFirePosY.SetName("Fire site y positions");
            histFirePosY.Init(0.0, 100, mSize * TILE_SIZE / 100.0 + 1);
            */

            //mFirePos.clear();
            mFireStats.clear();
            //mFireStats = std::vector<GraphicFireStats>(mSizeInBuckets * mSizeInBuckets);
            mFireStats = std::vector<GraphicFireStats>((NUM_BUCKETS + 0) * (NUM_BUCKETS + 0));
            //mFireStats.resize(mSizeInBuckets * mSizeInBuckets);
            //std::fill(mFireStats.begin(), mFireStats.end(), GraphicFireStats());

            statTrucksLeft = 0;
            statTrucksReturned = 0;
            statNumFires = 0;
            statBurnedDown = 0;

            mDispatcher = nullptr;
            mFireSiteGenerator = nullptr;
            mFireSites.SetName("Active fires");
            mFireSites.Clear();
        }

        void Map::printStatistics()
        {
            // Statistics printed to the terminal.
            fireSites().Output();
            statTrucksDriving.Output();
            statTrucksOutOfBase.Output();
            statTrucksAtBase.Output();
            statExtinguishLength.Output();
            statTimeBeforeArrival.Output();
            statTimeBeforeAvailable.Output();
            statTimeBetweenFires.Output();
            statTimeToAction.Output();
            statTimeFromDispatchToAction.Output();
            statFireTruckLate.Output();
            statDamage.Output();
            statDamagePrevented.Output();
            statDamagePercentage.Output();
            statValue.Output();
            for (Stat &stat : statsFireBurnLength)
            {
                stat.Output();
            }
            for (Stat &stat : statsTruckArrived)
            {
                stat.Output();
            }

            statYearsPassed = Time / HRS_PER_DAY / DAYS_PER_YEAR;
            statCostOfInfrastructure = statYearsPassed * mFireTrucks.size() * TRUCK_COST_PER_YEAR;
            statCostOfAction = statTrucksOutOfBase.Sum() * TRUCK_ACTION_COST_PER_HOUR;
            statTotalCost = statCostOfInfrastructure + statCostOfAction;

            Print("Total cost of fs system : \t\t%e Kc\n", statTotalCost);
            Print("Total cost of fs system per year : \t%e Kc\n", statTotalCost / statYearsPassed);

            statTotalDamage = statDamage.Sum();
            statTotalDamagePerYear = statTotalDamage / statYearsPassed;
            statTotalDamagePrevented = statDamagePrevented.Sum();
            statTotalDamagePreventedPerYear = statTotalDamagePrevented / statYearsPassed;

            Print("Total fire damage : \t\t\t%e Kc\n", statTotalDamage);
            Print("Total fire damage per year : \t\t%e Kc\n", statTotalDamagePerYear);

            Print("Total fire damage prevented : \t\t%e Kc\n", statTotalDamagePrevented);
            Print("Total fire damage prevented per year : \t%e Kc\n", statTotalDamagePreventedPerYear);

            statBudgetSum = statTotalDamagePrevented - statTotalDamage - statTotalCost;

            Print("Total budget sum : \t\t\t%e Kc\n", statBudgetSum);
            Print("Total budget sum per year : \t\t%e Kc\n", statBudgetSum / statYearsPassed);

            Print("Percentage of late trucks : \t\t%3.3f %%\n", (static_cast<double>(statFireTruckLate.Number()) /
                statNumFires) * 100.0);

            Print("Runtime : \t\t\t\t%3.1f years\n", statYearsPassed);

            Print("+----------------------------------------------------------+\n");

            Print("Trucks left %ld / returned %ld\n", map.statTrucksLeft, map.statTrucksReturned);
            Print("Fires started :  %ld\n", map.statNumFires);
            Print("Damage 100%% : %ld\n", map.statBurnedDown);
            Print("Size of a bucket : %f m\n", mSizeOfBucket);

            Print("+----------------------------------------------------------+\n");
            Print("Fire truck usage statistics : \n");

            double usageSum{0.0};

            for (FireTruck *truck : mFireTrucks)
            {
                double val{(truck->statTimeOutOfBase.Sum() / Time) * 100.0};
                usageSum += val;
                Print("\t%3.6f %%\n", val);
            }

            Print("Average usage per truck : %3.6f %%\n", usageSum / mFireTrucks.size());

            Print("+----------------------------------------------------------+\n");

            // Statistics printed to files.

            try { // Fire station positions.
                std::ofstream fireStationPositions("fireStationPositions.dat");
                for (FireTruck *truck : fireTrucks())
                {
                    fireStationPositions << truck->basePosition().x / mSizeOfBucket << " " <<
                                            truck->basePosition().y / mSizeOfBucket << std::endl;
                }
            } catch(...) {
                Print("Unable to save fireStationPositions.dat");
            }

            try { // Fire station real positions.
                std::ofstream fireStationPositions("fireStationRealPositions.dat");
                for (FireTruck *truck : fireTrucks())
                {
                    fireStationPositions << truck->basePosition().x << " " <<
                                         truck->basePosition().y << std::endl;
                }
            } catch(...) {
                Print("Unable to save fireStationRealPositions.dat");
            }

            try { // Statistics of fire site positions.
                std::ofstream fireHeatMap("fireHeatMap.dat");

                // Create a border of 0's around the data.
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    fireHeatMap << "0 ";
                }
                fireHeatMap << "\n";
                for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                {
                    if ((iii +1) % NUM_BUCKETS == 1)
                    {
                        fireHeatMap << "0 ";
                    }
                    fireHeatMap << mFireStats[iii].numFires << " ";
                    if ((iii + 1) % NUM_BUCKETS == 0)
                    {
                        fireHeatMap << "0\n";
                    }
                }
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    fireHeatMap << "0 ";
                }
                fireHeatMap << std::endl;
            } catch(...) {
                Print("Unable to save fireHeatMap.dat");
            }

            try { // Statistics how long it took before arrival.
                std::ofstream outFile("fireArrivalMaxMap.dat");

                // Create a border of 0's around the data.
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << "\n";
                for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                {
                    if ((iii +1) % NUM_BUCKETS == 1)
                    {
                        outFile << "0 ";
                    }
                    outFile << mFireStats[iii].timeBeforeAllArrived.Max() << " ";
                    if ((iii + 1) % NUM_BUCKETS == 0)
                    {
                        outFile << "0\n";
                    }
                }
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << std::endl;
            } catch(...) {
                Print("Unable to save fireArrivalMaxMap.dat");
            }

            try { // Statistics how long it took before arrival.
                std::ofstream outFile("fireArrivalMeanMap.dat");

                // Create a border of 0's around the data.
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << "\n";
                for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                {
                    if ((iii +1) % NUM_BUCKETS == 1)
                    {
                        outFile << "0 ";
                    }
                    if (mFireStats[iii].timeBeforeAllArrived.Number())
                    {
                        outFile << mFireStats[iii].timeBeforeAllArrived.MeanValue() << " ";
                    }
                    else
                    {
                        outFile << "0 ";
                    }
                    if ((iii + 1) % NUM_BUCKETS == 0)
                    {
                        outFile << "0\n";
                    }
                }
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << std::endl;
            } catch(...) {
                Print("Unable to save fireArrivalMeanMap.dat");
            }

            try { // Statistics how long it took before all trucks were dispatched.
                std::ofstream outFile("fireAllDispatchedMeanMap.dat");

                // Create a border of 0's around the data.
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << "\n";
                for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                {
                    if ((iii +1) % NUM_BUCKETS == 1)
                    {
                        outFile << "0 ";
                    }
                    if (mFireStats[iii].timeBeforeAllArrived.Number())
                    {
                        outFile << mFireStats[iii].timeCapFilled.MeanValue() << " ";
                    }
                    else
                    {
                        outFile << "0 ";
                    }
                    if ((iii + 1) % NUM_BUCKETS == 0)
                    {
                        outFile << "0\n";
                    }
                }
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << std::endl;
            } catch(...) {
                Print("Unable to save fireAllDispatchedMeanMap.dat");
            }

            try { // Statistics mean time of extinguishing.
                std::ofstream outFile("fireExtinguishMeanMap.dat");

                // Create a border of 0's around the data.
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << "\n";
                for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                {
                    if ((iii +1) % NUM_BUCKETS == 1)
                    {
                        outFile << "0 ";
                    }
                    if (mFireStats[iii].timeBeforeAllArrived.Number())
                    {
                        outFile << mFireStats[iii].timeExtinguish.MeanValue() << " ";
                    }
                    else
                    {
                        outFile << "0 ";
                    }
                    if ((iii + 1) % NUM_BUCKETS == 0)
                    {
                        outFile << "0\n";
                    }
                }
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << std::endl;
            } catch(...) {
                Print("Unable to save fireExtinguishMeanMap.dat");
            }

            try { // Statistics number of fires with by intensity.
                for (std::size_t intensity = 0; intensity < MAX_INTENSITY; ++intensity)
                {
                    std::stringstream ss;
                    ss << "fireByIntensity" << intensity + 1 << "Map.dat";
                    std::ofstream outFile(ss.str());

                    // Create a border of 0's around the data.
                    for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                    {
                        outFile << "0 ";
                    }
                    outFile << "\n";
                    for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                    {
                        if ((iii +1) % NUM_BUCKETS == 1)
                        {
                            outFile << "0 ";
                        }
                        outFile << mFireStats[iii].numFiresByIntensity[intensity] << " ";
                        if ((iii + 1) % NUM_BUCKETS == 0)
                        {
                            outFile << "0\n";
                        }
                    }
                    for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                    {
                        outFile << "0 ";
                    }
                    outFile << std::endl;
                }
            } catch(...) {
                Print("Unable to save fireByIntensityMap.dat");
            }

            try { // Statistics of how long fires burned before extinguished.
                std::ofstream outFile("fireBurnedMeanMap.dat");

                // Create a border of 0's around the data.
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << "\n";
                for (std::size_t iii = 0; iii < mFireStats.size(); ++iii)
                {
                    if ((iii +1) % NUM_BUCKETS == 1)
                    {
                        outFile << "0 ";
                    }
                    if (mFireStats[iii].timeBurned.Number())
                    {
                        outFile << mFireStats[iii].timeBurned.MeanValue() << " ";
                    }
                    else
                    {
                        outFile << "0 ";
                    }
                    if ((iii + 1) % NUM_BUCKETS == 0)
                    {
                        outFile << "0\n";
                    }
                }
                for (std::size_t iii = 0; iii < NUM_BUCKETS + 2; ++iii)
                {
                    outFile << "0 ";
                }
                outFile << std::endl;
            } catch(...) {
                Print("Unable to save fireBurnedMeanMap.dat");
            }
        }

        void Map::dispatch()
        {
            mDispatcher->Activate();
        }

        void Map::addFireSite(FireSite *site)
        {
            statNumFires++;
            DPrint("Adding new fire site (%p)...\n", site);

            //fireSites().InsLast(site);
            fireSites().Insert(site);
            dispatch();
        }

        Point Map::uniPoint() const
        {
            return Point {static_cast<std::size_t>(Uniform(0.0, mSize)),
                          static_cast<std::size_t>(Uniform(0.0, mSize))};
        }

        void Map::returnFireTruck(FireTruck *truck)
        {
            DPrint("Returned truck...\n");
            freeFireTrucks().push_back(truck);
            mDispatcher->Activate();
        }

        Map &Map::instance()
        {
            static Map map;
            return map;
        }

        double Map::fireDelay() const
        {
            return mFireDelay;
        }

        Map::~Map()
        {
            freeDynamics();
        }

        GraphicFireStats &Map::getFireStats(const FireSite *site)
        {
            std::size_t index{static_cast<std::size_t>(site->position().y / mSizeOfBucket) * NUM_BUCKETS +
                              static_cast<std::size_t>(site->position().x / mSizeOfBucket)};
            assert(index < mFireStats.size());
            return mFireStats[index];
        }

        void Map::prepareMembers(std::size_t mapSize, double firesPerYear, double originalMapArea)
        {
            mFiresPerYear = firesPerYear;
            mOriginalMapArea = originalMapArea;
            mSize = mapSize;
            //mSizeInBuckets = static_cast<std::size_t>(mSize / FIRE_POS_BUCKET_SIZE) + 1;
            mSizeOfBucket = static_cast<double>(mSize) / NUM_BUCKETS;

            // Calculate mean time between fires.
            double origFiresPerHour = mFiresPerYear / DAYS_PER_YEAR / HRS_PER_DAY;
            double mapSizeInKm = mSize * TILE_SIZE / 1000.0;
            double areaRatio = mOriginalMapArea / (mapSizeInKm * mapSizeInKm);
            mFireDelay =  1.0 / (origFiresPerHour / areaRatio);

            resetStatistics();

            freeDynamics();
        }

        void Map::createEvents()
        {
            // After everything is initialized, construct dispatcher and generator.
            mDispatcher = new Dispatcher();
            mFireSiteGenerator = new FireSiteGenerator();

            mFireSiteGenerator->Activate();
        }
    }
}
