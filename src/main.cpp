/**
 * @project VUT FIT IMS-2016
 * @file main.cpp
 * @author Tomas Polasek, Jan Pokorny
 * @brief Main source file for the simulation project for IMS.
 */

#include "main.h"

/**
 * Evaluate statistics from the current simulation.
 * @return Bigger value is worse.
 */
double valuate()
{
    double val{0.0};

    // Template:
    // val += fs::g::map....VALUE * CONSTANT

    val += fs::g::map.statTimeBeforeAvailable.Max() * 100.0;

    val += fs::g::map.statFireTruckLate.Number() * 10.0;

    val += -fs::g::map.statBudgetSum / 100000.0;

    return val;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "Wrong number of parameters.\n"
                  << "Usage: \n"
                  << "./simulator <1, 5>" << std::endl;
        return -1;
    }

    int choice{std::stoi(argv[1])};

    // Area of the map [km^2].
    static constexpr double MAP_AREA{103.36};
    // Length of one side of the map [m].
    static constexpr std::size_t MAP_SIDE_LENGTH{static_cast<std::size_t>(std::sqrt(MAP_AREA) * 1000.0)};
    // Runtime in years.
    static constexpr double RUNTIME_YEARS{100};
    // Number of fires per year on tested map.
    static constexpr double FIRES_PER_YEAR{240};
    // Seed for current simulation.
    static std::size_t SEED{static_cast<std::size_t>(time(nullptr))};
    std::size_t middle{static_cast<std::size_t>(MAP_SIDE_LENGTH / 2.0)};

    std::vector<fs::Point> fireTruckPositions;

    switch (choice)
    {
        case 1:
        {
            fireTruckPositions = {
                fs::Point(middle, middle),
                fs::Point(middle, middle),
                fs::Point(middle, middle),

                fs::Point(middle, middle),
                fs::Point(middle, middle),
                fs::Point(middle, middle),

                fs::Point(middle, middle),
                fs::Point(middle, middle),
                fs::Point(middle, middle),

                fs::Point(middle, middle),
                fs::Point(middle, middle),
                fs::Point(middle, middle),
            };
            break;
        }
        case 2:
        {
            fireTruckPositions = {
                fs::Point(middle - 2000, middle),
                fs::Point(middle - 2000, middle),
                fs::Point(middle - 2000, middle),

                fs::Point(middle - 2000, middle),
                fs::Point(middle - 2000, middle),
                fs::Point(middle - 2000, middle),

                fs::Point(middle + 2000, middle),
                fs::Point(middle + 2000, middle),
                fs::Point(middle + 2000, middle),

                fs::Point(middle + 2000, middle),
                fs::Point(middle + 2000, middle),
                fs::Point(middle + 2000, middle),
            };
            break;
        }
        case 3:
        {
            fireTruckPositions = {
                fs::Point(middle - 2000, middle - 2000),
                fs::Point(middle - 2000, middle - 2000),
                fs::Point(middle - 2000, middle - 2000),

                fs::Point(middle - 2000, middle + 2000),
                fs::Point(middle - 2000, middle + 2000),
                fs::Point(middle - 2000, middle + 2000),

                fs::Point(middle + 2000, middle - 2000),
                fs::Point(middle + 2000, middle - 2000),
                fs::Point(middle + 2000, middle - 2000),

                fs::Point(middle + 2000, middle + 2000),
                fs::Point(middle + 2000, middle + 2000),
                fs::Point(middle + 2000, middle + 2000),
            };
            break;
        }
        case 4:
        {
            fireTruckPositions = {
                fs::Point(middle - 2000, middle - 2000),
                fs::Point(middle - 2000, middle - 2000),

                fs::Point(middle - 2000, middle + 2000),
                fs::Point(middle - 2000, middle + 2000),

                fs::Point(middle + 2000, middle - 2000),
                fs::Point(middle + 2000, middle - 2000),
                fs::Point(middle + 2000, middle - 2000),

                fs::Point(middle + 2000, middle + 2000),
                fs::Point(middle + 2000, middle + 2000),
            };
            break;
        }
        case 5:
        {
            fireTruckPositions = {
                fs::Point(middle, middle + 3000),
                fs::Point(middle, middle + 3000),
                fs::Point(middle, middle + 3000),

                fs::Point(middle - 2000, middle - 2000),
                fs::Point(middle - 2000, middle - 2000),
                fs::Point(middle - 2000, middle - 2000),

                fs::Point(middle + 2000, middle - 2000),
                fs::Point(middle + 2000, middle - 2000),
                fs::Point(middle + 2000, middle - 2000),
            };
            break;
        }
        default:
        {
            std::cout << "Unknown number option.\n"
                      << "Usage: \n"
                      << "./simulator <1, 5>" << std::endl;
            return -1;
        }
    }

    RandomSeed(SEED);

    Init(0.0, fs::SIM_END_TIME * RUNTIME_YEARS);

    fs::g::map.init(MAP_SIDE_LENGTH, FIRES_PER_YEAR, MAP_AREA, fireTruckPositions);

    Run();

    fs::g::map.printStatistics();
    Print("Seed is : %ld, choice : %d\n", SEED, choice);
    SIMLIB_statistics.Output();
}
