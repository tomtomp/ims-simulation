/**
 * @file FireStation.cpp
 * @author Tomas Polasek
 * @brief This process represents a fire station.
 */

#include <Globals.h>
#include "FireStation.h"

namespace fs
{
    FireStation::FireStation(Point pos) :
        mPosition(pos), mTruck(pos), mFireSite{nullptr}
    {

    }

    void FireStation::Behavior()
    {
        double now{Time};
        Wait(mTruck.travelTime());
        g::truckDriveStat(Time - now);

        mFireSite->arrived();
    }

    void FireStation::dispatch(FireSite *site)
    {
        if (site->trucksRequired() == 0)
        {
            throw std::runtime_error("Cannot assist already full fire site!");
        }

        mFireSite = site;
        mTruck.setDestination(site->position());
        site->addParticipant(this);
        Activate();
    }
}
