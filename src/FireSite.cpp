/**
 * @project VUT FIT IMS-2016
 * @file FireSite.cpp
 * @author Tomas Polasek, Jan Pokorny
 * @brief This process represents a single fire.
 */

#include "FireSite.h"

namespace fs
{
    BurnDownTimer::BurnDownTimer(double time, FireSite *site) :
        mFireSite{site}
    {
        Activate(Time + time);
    }

    void BurnDownTimer::Behavior()
    {
        mFireSite->burnOut();
    }

    FireSite::FireSite(Point pos, std::size_t intensity, double value, double discDelta) :
        mPosition(pos), mIntensity{intensity}, mValue{value},
        mTrucksArrived{0}, mTrucksReady{0}, mFireDiscoveryDelta{discDelta}
    {
        assert(mIntensity > 0 && mIntensity <= MAX_INTENSITY);
        //Priority = static_cast<Priority_t >(mIntensity);
        mFireStartTime = Time;
    }

    void FireSite::burnOut()
    {

    }

    std::size_t FireSite::trucksRequired() const
    {
        if (mParticipants.size() > mIntensity)
        {
            throw std::runtime_error("More participants than the size of the fire!");
        }

        return mIntensity - mParticipants.size();
    }

    void FireSite::Behavior()
    {
        const double intensityIncStep{INTENSITY_INC_STEP[mIntensity]};
        double intensityVal{mIntensity * INTENSITY_STEP};
        std::size_t minutesToSimulate{0};

        double damagePercentage{0};

        minutesToSimulate = static_cast<std::size_t>(mFireDiscoveryDelta * MINUTES_IN_HOUR + 1.0);
        // Calculate fire damage before discovery of the fire.
        for (std::size_t iii = 0; iii < minutesToSimulate; ++iii)
        {
            damagePercentage += (intensityVal * DAMAGE_PER_INTENSITY);
            intensityVal += intensityIncStep;
        }

        DPrint("Fire started (%p), require %d fire trucks.\n", this, mIntensity);

        double lastTime{Time};
        std::size_t lastTrucks{mTrucksReady};

        while (mTrucksReady < mIntensity)
        { // Wait for "mIntensity" fire trucks.
            DPrint("Fire trucks needed : %d %p\n", mIntensity - mTrucksReady, this);

            Passivate();

            // Add damage.
            // Let the fire intensity ramp up.
            // Lower the intensity, using the fire trucks.
            minutesToSimulate = static_cast<std::size_t>((Time - lastTime) * MINUTES_IN_HOUR + 1.0);
            assert(mTrucksReady >= 1);
            // Calculate fire damage before discovery of the fire.
            for (std::size_t iii = 0; iii < minutesToSimulate; ++iii)
            {
                damagePercentage += (intensityVal * DAMAGE_PER_INTENSITY);
                intensityVal += intensityIncStep - INTENSITY_DEC_STEP[lastTrucks];
            }

            // Remember current state.
            lastTime = Time;
            lastTrucks = mTrucksReady;
        }

        assert(intensityVal > 0.0);

        assert(mTrucksReady == mIntensity);
        double intensityDelta{intensityIncStep - INTENSITY_DEC_STEP[mTrucksReady]};

        minutesToSimulate = 0;
        while (intensityVal > 0.0)
        {
            damagePercentage += (intensityVal * DAMAGE_PER_INTENSITY);
            intensityVal += intensityDelta;
            minutesToSimulate++;
        }

        // In hours
        //double timeToExtinguish{(-intensityVal / intensityDelta) / 60.0};
        double timeToExtinguish{minutesToSimulate / MINUTES_IN_HOUR};

        DPrint("Extinguishing... %p\n", this);

        double extinguishStartTime = Time;

        //Wait(Exponential(mIntensity * FIRE_BURN_TIME_MODIFIER));
        Wait(timeToExtinguish);

        // Check that the fire has been actually extinguished.
        //assert(std::abs(intensityVal) <= -intensityDelta);

        g::map.statExtinguishLength(Time - extinguishStartTime);
        g::map.statsFireBurnLength[mIntensity - 1](Time - mFireStartTime);
        g::map.statTimeBeforeArrival(extinguishStartTime - mFireStartTime);
        g::map.statTimeBeforeAvailable(mCapFilledTime - mFireStartTime);

        DPrint("Sending trucks back...\n");
        for (FireTruck* truck : mParticipants)
        {
            truck->returnToBase();
        }

        DPrint("Fire site ending.\n");

        auto &stats(g::map.getFireStats(this));

        stats.numFires++;
        assert(mIntensity > 0 && mIntensity <= MAX_INTENSITY);
        stats.numFiresByIntensity[mIntensity - 1]++;
        assert((Time - mFireStartTime) > 0.0);
        stats.timeBurned(Time - mFireStartTime);
        assert((mCapFilledTime - mFireStartTime) >= 0.0);
        stats.timeCapFilled(mCapFilledTime - mFireStartTime);
        assert((Time - extinguishStartTime) > 0.0);
        stats.timeExtinguish(Time - extinguishStartTime);
        assert((extinguishStartTime - mFireStartTime) > 0.0);
        stats.timeBeforeAllArrived(extinguishStartTime - mFireStartTime);

        //double fireLength{mFireDiscoveryDelta + Time - mFireStartTime};
        //double damagePercentage{1.0 - std::exp(-0.05268 * fireLength)};
        if (damagePercentage >= 100.0)
        {
            g::map.statBurnedDown++;
            damagePercentage = 100.0;
        }

        g::map.statDamagePercentage(damagePercentage);

        double damageDone{mValue * damagePercentage / 100.0};
        double damagePrevented{mValue - damageDone};
        if (damagePrevented < 0.0)
        {
            damagePrevented = 0.0;
        }

        stats.damage(damageDone);
        stats.preventedDamage(damagePrevented);
        g::map.statDamage(damageDone);
        g::map.statDamagePrevented(damagePrevented);
        g::map.statValue(mValue);

        Cancel();
    }

    void FireSite::addParticipant(FireTruck *station)
    {
        if (mParticipants.size() >= mIntensity)
        {
            throw std::runtime_error("More participants than the size of the fire!");
        }

        mParticipants.push_back(station);

        if (mParticipants.size() == mIntensity)
        {
            mCapFilledTime = Time;
        }
    }

    void FireSite::truckArrived()
    {
        double arrived{Time - mFireStartTime};
        // Remember times when the new trucks arrived.
        g::map.statsTruckArrived[mTrucksArrived](arrived);
        if (arrived > TRUCK_ARRIVAL_THRESHOLDS[mTrucksArrived])
        { // The truck arrived late.
            g::map.statFireTruckLate(arrived - TRUCK_ARRIVAL_THRESHOLDS[mTrucksArrived]);
        }

        mTrucksArrived++;
    }

    void FireSite::truckReady()
    {
        mTrucksReady++;

        Activate();
    }
}
