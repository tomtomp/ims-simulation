# Set output file.
set terminal png
set size 0.90, 1.0
#set output "fireHeatMap.gif"

# Interpolation between buckets.
set pm3d map
set pm3d interpolate 0,0

# Style for fire station points.
set style line 1 lc rgb 'green' pt 5

# Grid lines
set ytics 0,1,10
set xtics 0,1,10
set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"

# Axis description
set xlabel "X coordinate [Tiles]"
set ylabel "Y coordinate [Tiles]"
set cblabel "Fires of intensity 3 [fires]"

#splot 'fireHeatMap.dat' matrix, 'fireStationPositions.dat' w p ls 1
#splot 'fireHeatMap.dat' with image notitle, 'fireStationPositions.dat' using 1:2:(0) with points ls 1 notitle
#plot 'fireHeatMap.dat' with image, 'fireStationPositions.dat' using 1:2:(0) with points
splot '<cat' matrix using ($1 - 0.5):($2 - 0.5):3 notitle, 'fireStationPositions.dat' using 1:2:(0) with points ls 1 notitle
