#!/bin/bash

SCRIPT_FOLDER="plotScripts"
OUTPUT_FOLDER="graphs"
SCRIPTS="fireHeatMap fireArrivalMaxMap fireArrivalMeanMap fireAllDispatchedMeanMap fireExtinguishMeanMap fireByIntensity1Map fireByIntensity2Map fireByIntensity3Map fireBurnedMeanMap"

for name in `echo ${SCRIPTS}`
do
    echo "Creating graph : ${name}"
    cat "${name}.dat" | gnuplot "${SCRIPT_FOLDER}/${name}.plot" > "${OUTPUT_FOLDER}/${name}.png"
done
