\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}\IeC {\'U}vod}{2}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Kdo se na pr\IeC {\'a}ci pod\IeC {\'\i }lel}{2}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}V jak\IeC {\'e}m prost\IeC {\v r}ed\IeC {\'\i }}{2}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Rozbor t\IeC {\'e}matu a pou\IeC {\v z}it\IeC {\'y}ch metod/technologi\IeC {\'\i }}{2}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Popis pou\IeC {\v z}it\IeC {\'y}ch technologi\IeC {\'\i }}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Koncepce - model\IeC {\'a}\IeC {\v r}sk\IeC {\'a} t\IeC {\'e}mata}{4}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Konceptu\IeC {\'a}ln\IeC {\'\i } model}{5}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Koncepce - implementa\IeC {\v c}n\IeC {\'\i } t\IeC {\'e}mata}{6}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Architektura simula\IeC {\v c}n\IeC {\'\i }ho modelu/simul\IeC {\'a}toru}{7}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Podstata simula\IeC {\v c}n\IeC {\'\i }ch experiment\IeC {\r u} a jejich pr\IeC {\r u}b\IeC {\v e}h}{7}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Postup experimentov\IeC {\'a}n\IeC {\'\i }}{8}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Dokumentace jednotliv\IeC {\'y}ch experiment\IeC {\r u}}{8}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Z\IeC {\'a}v\IeC {\v e}ry experiment\IeC {\r u}}{14}{subsection.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Shrnut\IeC {\'\i } simula\IeC {\v c}n\IeC {\'\i }ch experiment\IeC {\r u} a z\IeC {\'a}v\IeC {\v e}r}{15}{section.7}
