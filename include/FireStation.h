/**
 * @file FireStation.h
 * @author Tomas Polasek
 * @brief This process represents a fire station.
 */

#ifndef SIMULATION_FIRESTATION_H
#define SIMULATION_FIRESTATION_H

#include "Types.h"
#include "FireTruck.h"
#include "FireSite.h"

namespace fs
{
    /**
     * Each fire station has a single fire fighting truck.
     */
    class FireStation : public Process
    {
    public:
        /**
         * Construct a fire station on given coordinates.
         * @param pos Position of the fire station.
         */
        explicit FireStation(Point pos);

        /**
         * Dispatch truct inside this station to given fire site.
         * @param site Where should the truck drive to.
         */
        void dispatch(FireSite *site);

        /// Overriding behavior of this process.
        void Behavior() override final;

        /// Is the fire truck belonging to this station available?
        bool available() const
        { return mTruck.atBase(); }

        /// Get position of this fire station.
        const Point &position() const
        { return mPosition; }
    private:
        /// Position of the fire station.
        Point mPosition;
        /// Truck belonging to this station.
        FireTruck mTruck;
        /// Fire being extinguished by this station.
        FireSite *mFireSite;
    protected:
    };
}

#endif //SIMULATION_FIRESTATION_H
