/**
 * @project VUT FIT IMS-2016
 * @file FireTruck.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief This class represents fire fighting truck.
 */

#ifndef SIMULATION_FIRETRUCK_H
#define SIMULATION_FIRETRUCK_H

#include "Types.h"
#include "FireSite.h"

namespace fs
{
    class FireSite;

    class FireTruck : public Process
    {
    public:
        /**
         * Construct a truck in given position.
         * @param basePos Position of the base, this truck belongs to.
         */
        explicit FireTruck(Point basePos);

        /**
         * Calculate the travel time from current position to the destination position.
         * @return Time before reaching the destination.
         */
        double travelTime()
        { return mCurPosition.distance(mDestination) / TRUCK_SPEED; }

        /**
         * Predict, how long will the truck travel to given point.
         * @param p Destination.
         * @return Time it would take [h] .
         */
        double predictTravelTime(const Point &p)
        { return mCurPosition.distance(p) / TRUCK_SPEED; }

        /**
         * Dispatch truct inside this station to given fire site.
         * @param site Where should the truck drive to.
         */
        void dispatch(FireSite *site);

        /**
         * Return this truck to base.
         */
        void returnToBase();

        /// Overriding behavior of this process.
        void Behavior() override final;

        /**
         * Set the destination of this truck.
         * @param dest Where is this truck driving to.
         */
        void setDestination(const Point &dest)
        { mDestination = dest; }

        /// Is this truck at its fire station?
        bool atBase() const
        { return mAtBase; }

        /// Get current position of this truck.
        Point position() const
        { return mCurPosition; }

        /// Get position of the base for this truck.
        Point basePosition() const
        { return mBasePosition; }

        /// How lond has this truck been out of base in total?
        Stat statTimeOutOfBase;
    private:
        /// Position of the base for this truck.
        Point mBasePosition;
        /// Current location of this truck.
        Point mCurPosition;
        /// Destination of this truck.
        Point mDestination;
        /// Is this truck parked at the home base?
        bool mAtBase;
        /// Fire site this truck is extinguishing.
        FireSite *mFireSite;
    protected:
    };
}

#endif //SIMULATION_FIRETRUCK_H
