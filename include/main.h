/**
 * @project VUT FIT IMS-2016
 * @file main.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief Main header for the simulation project for IMS.
 */

#ifndef SIMULATION_MAIN_H
#define SIMULATION_MAIN_H

#include <iostream>

#include "Globals.h"

#endif //SIMULATION_MAIN_H
