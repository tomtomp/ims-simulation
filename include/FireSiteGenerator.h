/**
 * @project VUT FIT IMS-2016
 * @file FireSiteGenerator.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief Generator for the fire sites.
 */

#ifndef SIMULATION_FIRESITEGENERATOR_H
#define SIMULATION_FIRESITEGENERATOR_H

#include "Types.h"
#include "FireSite.h"
#include "Globals.h"

namespace fs
{
    class FireSiteGenerator : public Event
    {
    public:
        /**
         * Create a fire site generator.
         */
        explicit FireSiteGenerator();

        /// Implementing behavior of this event.
        void Behavior() override final;
    private:
        /**
         * Generate intensity for a fire site.
         * @return Intensity <1, 3>.
         */
        std::size_t genIntensity() const;

        /**
         * Generate a value of a burning property.
         * @return The value of the property on fire.
         */
        double genValue() const;

        /**
         * Generate a value of when the time has been discovered.
         * @return The delta value from current time.
         */
        double genDiscoveryDelta() const;
    protected:
    };
}

#endif //SIMULATION_FIRESITEGENERATOR_H
