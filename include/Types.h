/**
 * @project VUT FIT IMS-2016
 * @file Types.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief Type definitions.
 */

#ifndef SIMULATION_TYPES_H
#define SIMULATION_TYPES_H

#include <vector>
#include <queue>
#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include <cassert>
#include <unordered_map>

#include <simlib.h>

#include "Constants.h"

namespace fs
{
#ifdef DEBUG_PRINT
    #define DPrint(...) Print(__VA_ARGS__)
#else
    #define DPrint(...)
#endif

    /**
     * Point on a 2D map.
     */
    struct Point
    {
        explicit Point(std::size_t pX, std::size_t pY) :
            x(pX), y(pY)
        { }

        /**
         * Get distance from another point using the Pythagorian
         * theorem.
         * @param other The other point.
         * @return Returns the distance of this point from the other one.
         */
        double distance(const Point &other) const
        {
            const double distX{static_cast<double>(x) - other.x};
            const double distXSq{distX * distX};
            const double distY{static_cast<double>(y) - other.y};
            const double distYSq{distY * distY};
            return std::sqrt(distXSq + distYSq);
        }

        /// X coordinate
        std::size_t x;
        /// Y coordinate
        std::size_t y;
    };

    /*
     * Solution for mapping Points to numbers found on :
     * http://stackoverflow.com/questions/16792751/hashmap-for-2d3d-coordinates-i-e-vector-of-doubles
     */

    /**
     * Hash functor for Point class.
     */
    struct PointHash
    {
        std::size_t operator()(const Point &p) const
        {
            std::size_t hashX{std::hash<std::size_t>()(p.x)};
            std::size_t hashY{std::hash<std::size_t>()(p.y)};

            return (hashX ^ (hashY << 1));
        }
    };

    /**
     * Equals functor for Point class.
     */
    struct PointEquals
    {
        bool operator()(const Point &p1, const Point &p2) const
        {
            return (p1.x == p2.x) && (p1.y == p2.y);
        }
    };

    /// Map used for mapping of points to numbers.
    using PointMap = std::unordered_map<Point, std::size_t, PointHash, PointEquals>;
}

#endif //SIMULATION_TYPES_H
