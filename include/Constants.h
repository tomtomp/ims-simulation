/**
 * @project VUT FIT IMS-2016
 * @file Constants.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief Header containing constants required by the simulation.
 */

#ifndef SIMULATION_CONSTANTS_H
#define SIMULATION_CONSTANTS_H

#include <string>

namespace fs
{
    /// Number of hours per day.
    static constexpr double HRS_PER_DAY{24.0};
    /// Number of days per year.
    static constexpr double DAYS_PER_YEAR{365.25};
    /// Number of minutes in one hour.
    static constexpr double MINUTES_IN_HOUR{60.0};
    /// Time, when the simulation should end (one year in hours).
    static constexpr double SIM_END_TIME{DAYS_PER_YEAR * HRS_PER_DAY};
    /// Size of a map tile [m].
    static constexpr double TILE_SIZE{1.0};
    /// Average fire truck speed in [m/h].
    static constexpr double TRUCK_SPEED{45.0 * (1000.0 / TILE_SIZE)};
    /// Maximal intensity of a fire site.
    static constexpr std::size_t MAX_INTENSITY{3};
    /// Fire position statistic bucket size [m].
    //static constexpr double FIRE_POS_BUCKET_SIZE{1000};
    /// To how many buckets will the map be divided into.
    static constexpr std::size_t NUM_BUCKETS{10};
    /// What time is the limit of drive distance to a fire site [h].
    static constexpr double MAX_TRAVEL_TIME{2.0};
    /// Preparation time before the next action [h].
    static constexpr double PREP_TIME{0.5};
    /// Time, before the firefighters get ready to extinguish [h].
    static constexpr double TIME_TO_ACTION{0.0086};
    /// Time to pack up and get ready to leave the fire site [h].
    static constexpr double TIME_TO_PACK_UP{0.0086};
    /// Cost of one fire truck (fire team) [Kc/year].
    static constexpr double TRUCK_COST_PER_YEAR{7164000};
    /// Cost of one fire truck (fire team) per one hour of action [Kc/h].
    static constexpr double TRUCK_ACTION_COST_PER_HOUR{5600};
    /// Threshholds for arrival of fire trucks [h].
    static constexpr double TRUCK_ARRIVAL_THRESHOLDS[MAX_INTENSITY] =
    { // Values set for I-A .
        0.117, // 7 minutes
        0.117, // 7 minutes
        0.167  // 10 minutes
    };
    /// Average value of a fire site [Kc].
    static constexpr double AVG_VALUE_OF_SITE{678520};
    /// Average time of fire discovery [h].
    static constexpr double AVG_DISCOVERY_TIME{0.05};
    /// Multiplier for the fire intensity, value set, so
    /// that the fire truck out of base corresponded with reality.
    static constexpr double FIRE_BURN_TIME_MODIFIER{0.9};
    /// Intensity step size.
    static constexpr double INTENSITY_STEP{100.0};
    /// Intensity increase step, for each intensity level.
    static constexpr double INTENSITY_INC_STEP[MAX_INTENSITY + 1]
    {
        0.0, // Unused
        0.7, // Intensity 1
        3.4, // Intensity 2
        7.0  // Intensity 3
    };
    /// Intensity decrease step, for each fire truck.
    static constexpr double INTENSITY_DEC_STEP[MAX_INTENSITY + 1]
    {
        0.0, // Unused
        2.5, // 1 fire truck
        5.7, // 2 fire truck
        8.6  // 3 fire truck
    };
    /// Intensity dictates, how many fire trucks are required.
    //static_assert(INTENSITY_INC_STEP < INTENSITY_DEC_STEP, "Intensity increase has to be lower than decrease!");
    /// Value of percentual damage per intensity per minute.
    static constexpr double DAMAGE_PER_INTENSITY{0.0004};
}

#endif //SIMULATION_CONSTANTS_H
