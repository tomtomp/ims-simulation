/**
 * @project VUT FIT IMS-2016
 * @file FireSite.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief This process represents a single fire.
 */

#ifndef SIMULATION_FIRESITE_H
#define SIMULATION_FIRESITE_H

#include "Types.h"
#include "FireTruck.h"
#include "Globals.h"

namespace fs
{
    /**
     * Timer when the fire stops by itself.
     */
    class BurnDownTimer : public Event
    {
    public:
        /**
         * Crate a timeout, when the fire will burn itself out.
         * @param time Additive time in the future.
         * @param site Site corresponding to this timeout.
         */
        BurnDownTimer(double time, FireSite *site);

        /// Overriding the behavior of this event.
        void Behavior() override final;
    private:
        /// Corresponding site for this timeout.
        FireSite *mFireSite;
    protected:
    };

    class FireSite : public Process
    {
    public:
        /**
         * Construct a fire on given position.
         * @param pos Position of the fire.
         * @param intensity Intensity of this fire, also
         *   dictates the number of required trucks to
         *   extinguish.
         * @param value Value of the property on fire.
         * @param discDelta Delta time, when the fire was discovered.
         */
        explicit FireSite(Point pos, std::size_t intensity, double value, double discDelta);

        /// Overriding behavior of this process.
        void Behavior() override final;

        /**
         * Add participating fire station.
         * @param station Adding this fire station.
         */
        void addParticipant(FireTruck *station);

        /**
         * When a fire truck arrives, this function is called.
         */
        void truckArrived();

        /**
         * When a fire truck is ready for extinguishing.
         */
        void truckReady();

        /**
         * This site has not been extinguished in time
         * to save anything.
         */
        void burnOut();

        /// Get the position of this fire.
        const Point &position() const
        { return mPosition; }

        /// How many fire trucks are required at this moment?
        std::size_t trucksRequired() const;
    private:
        /// Position of the fire.
        Point mPosition;
        /// Intensity of this fire site.
        std::size_t mIntensity;
        /// Fire stations participating on extinguishing this fire.
        std::vector<FireTruck*> mParticipants;
        /// Value of the property.
        double mValue;
        /// How many trucks have arrived?
        std::size_t mTrucksArrived;
        /// How many trucks are ready for extinguishing?
        std::size_t mTrucksReady;
        /// Time when the fire sites capacity was filled (number of trucks are on the way).
        double mCapFilledTime;
        /// Time, when the fire started.
        double mFireStartTime;
        /// Delta time, when the fire was discovered.
        double mFireDiscoveryDelta;
    protected:
    };
}

#endif //SIMULATION_FIRESITE_H
