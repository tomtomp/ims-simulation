/**
 * @project VUT FIT IMS-2016
 * @file Dispatcher.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief Dispatcher for dispatching fire fighting trucks.
 */

#ifndef SIMULATION_DISPATCHER_H
#define SIMULATION_DISPATCHER_H

#include "FireSite.h"

namespace fs
{
    class Dispatcher : public Event
    {
    public:
        /**
         * Create a dispatcher.
         */
        explicit Dispatcher();

        /// Implementing behavior of this event.
        void Behavior() override final;
    private:
    protected:
    };
}

#endif //SIMULATION_DISPATCHER_H
