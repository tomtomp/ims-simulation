/**
 * @project VUT FIT IMS-2016
 * @file Globals.h
 * @author Tomas Polasek, Jan Pokorny
 * @brief Global variables.
 */

#ifndef SIMULATION_GLOBALS_H
#define SIMULATION_GLOBALS_H

#include <fstream>
#include <iostream>

#include "Types.h"

namespace fs
{
    class Dispatcher;
    class FireSiteGenerator;
    class FireSite;
    class FireTruck;

    namespace g
    {
        /// Statistics about a fire.
        struct GraphicFireStats
        {
            GraphicFireStats() :
                numFires{0}
            {
                std::fill(std::begin(numFiresByIntensity), std::end(numFiresByIntensity), 0);
            }

            /// How many fires were there in this bucket?
            std::size_t numFires;
            /// Number of fires by intensity
            std::size_t numFiresByIntensity[MAX_INTENSITY];
            /// How much damage was done?
            Stat damage;
            /// How much damage was pervented?
            Stat preventedDamage;
            /// How long did the fire burn before all required fire trucks were sent.
            Stat timeCapFilled;
            /// How long did it take for all the required trucks to arrive?
            Stat timeBeforeAllArrived;
            /// How long did the fire burn in total.
            Stat timeBurned;
            /// How long did the fire take to extinguish.
            Stat timeExtinguish;
        };

        /**
         * Map is a 2D rectangular grid, where each field
         * has a given size.
         * Each field can contain 0 or more fire stations
         * and 0 or more fire sites.
         */
        class Map
        {
        public:
            /// Get singleton instance
            static Map &instance();

            ~Map();

            /**
             * Create a map with given size and the number of fire stations.
             * @param mapSize Size of the square map in tiles.
             * @param firesPerYear The number of fires per year.
             * @param originalMapArea Area of the location, where firesPerYear
             *   statistic was taken from.
             * @param numStations Number of stations.
             */
            void init(std::size_t mapSize, double firesPerYear, double originalMapArea, std::size_t numStations);

            /**
             * Create a map with given size and the number of fire stations.
             * @param mapSize Size of the square map in tiles.
             * @param firesPerYear The number of fires per year.
             * @param originalMapArea Area of the location, where firesPerYear
             *   statistic was taken from.
             * @param stations Location of stations.
             */
            void init(std::size_t mapSize, double firesPerYear,
                      double originalMapArea, const std::vector<Point> &stations);

            /**
             * Print gathered statistics.
             */
            void printStatistics();

            /**
             * Activate the dispatcher.
             * Should be called every time a fire is created,
             * extinguished, or when a fire truck returns.
             */
            void dispatch();

            /**
             * Add a fire site to this map.
             * @param site Add this site.
             */
            void addFireSite(FireSite *site);

            /**
             * Return given fire truck to the list of available
             * fire trucks.
             * @param truck Add this fire truck.
             */
            void returnFireTruck(FireTruck *truck);

            /*
             * Get stats structure for given fire sites location.
             * @param site The fire site.
             * @return Stats structure for given fire site.
             */
            GraphicFireStats &getFireStats(const FireSite *site);

            /**
             * How long should generator wait between generating fire
             * sites.
             * @return How long in hours should the generator wait (mean value).
             */
            double fireDelay() const;

            /**
             * Generate a point on this map using
             * Uniform function.
             * @return Point on this map.
             */
            Point uniPoint() const;

            /// Get the size of this map.
            std::size_t size() const
            { return mSize; }

            /// Get queue of active fire sites.
            Queue &fireSites()
            { return mFireSites; }

            /// Get the list of free fire trucks.
            std::vector<FireTruck*> &freeFireTrucks()
            { return mFreeFireTrucks; }

            /// Get the list of all fire trucks.
            std::vector<FireTruck*> &fireTrucks()
            { return mFireTrucks; }

            // Statistic section :
            /// Statistic counting time the trucks spent driving.
            Stat statTrucksDriving;
            /// Statistic counting time the trucks spent outside of base.
            Stat statTrucksOutOfBase;
            /// Statistic counting time the trucks spent waiting at base.
            Stat statTrucksAtBase;
            /// Statistic counting how long does it take to extinguish a fire.
            Stat statExtinguishLength;
            /// Statistic counting how long does it take before fire trucks arrive.
            Stat statTimeBeforeArrival;
            /// Statistic counting how long does it take before all fire trucks become available.
            Stat statTimeBeforeAvailable;
            /// Statistics for how much time passed between starting fires.
            Stat statTimeBetweenFires;
            /// Statistics for how long each fire burned, sorted by intensity.
            Stat statsFireBurnLength[MAX_INTENSITY];
            /// Statistics for how long does it take for each required truck to arrive.
            Stat statsTruckArrived[MAX_INTENSITY];
            /// Statistics for how long does it take for the firemen to get reaady to extinguish.
            Stat statTimeToAction;
            /// Statistics for how long does it take between dispatch and action ready.
            Stat statTimeFromDispatchToAction;
            /// Statistics about by how much time a fire truck was late.
            Stat statFireTruckLate;
            /// Statistics about how much damage has the fire done.
            Stat statDamage;
            /// Statistics about how much damage has been prevented.
            Stat statDamagePrevented;
            /// Statitics about percentage of fire damage.
            Stat statDamagePercentage;
            /// Statistics about the value of the fire sites.
            Stat statValue;

            /// Statistic about how many trucks have left base.
            std::size_t statTrucksLeft;
            /// Statistic about how many trucks have returned to base.
            std::size_t statTrucksReturned;
            /// Statistic about how many fires were started.
            std::size_t statNumFires;
            /// How many fires had damage of 100%.
            std::size_t statBurnedDown;

            // Exposed statistics.
            double statYearsPassed;
            double statCostOfInfrastructure;
            double statCostOfAction;
            double statTotalCost;
            double statTotalDamage;
            double statTotalDamagePerYear;
            double statTotalDamagePrevented;
            double statTotalDamagePreventedPerYear;
            double statBudgetSum;
        private:
            Map()
            { }

            /**
             * Prepare member variables.
             */
            void prepareMembers(std::size_t mapSize, double firesPerYear, double originalMapArea);

            /**
             * Create events required for this simulation.
             */
            void createEvents();

            /**
             * Free dynamic structures.
             * Includes freeFireTrucks, dispatcher and fireSiteGenerator.
             */
            void freeDynamics();

            /**
             * Reset the internal statistics.
             */
            void resetStatistics();

            /// Names for fire intensity stats.
            static const char *INTENSITY_STAT_NAMES[];
            /// Names for truck arrival stats.
            static const char *TRUCK_ARRIVAL_STAT_NAMES[];

            /// How many fires were there per year.
            double mFiresPerYear;
            /// What are is the number of fires captured on [m^2].
            double mOriginalMapArea;

            /// Size of the map in tiles.
            std::size_t mSize;
            /// Size of the map in buckets.
            double mSizeOfBucket;

            /// Dispatcher for this map.
            Dispatcher *mDispatcher;
            /// Fire site generator for this map.
            FireSiteGenerator *mFireSiteGenerator;
            /// Active fire sites.
            Queue mFireSites;
            /// List of available fire trucks.
            std::vector<FireTruck*> mFreeFireTrucks;
            /// List of all fire trucks.
            std::vector<FireTruck*> mFireTrucks;
            /// Mean time for the delay between fires.
            double mFireDelay;
            /// Statistics for fire positions.
            std::vector<GraphicFireStats> mFireStats;
            //PointMap mFirePos;
        protected:
        };

        /// Global map object, needs to be init'ed.
        extern Map &map;
    }
}

#endif //SIMULATION_GLOBALS_H
